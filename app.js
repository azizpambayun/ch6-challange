const express = require("express");
const app = express();
const port = 3000;
const bodyParser = require("body-parser");

const loginRoute = require("./routes/login");
const dashboardRoute = require("./routes/dashboard");

app.use(bodyParser.json());
app.use(express.static("public"));

app.use("/login", loginRoute);
app.use("/dashboard", dashboardRoute);

app.set("view engine", "ejs");

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
